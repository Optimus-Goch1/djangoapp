import os

from setuptools import find_packages,setup

with open(os.path.join(os.path.dirname(__file__), "README.rst")) as readme:
    README = readme.read()

setup(
    name='django-ogochukwubahscrumy',
    version='0.1',
    packages= find_packages(),
    include_package_data=True,
    license='BSD license',
    long_description= README,
    author='Ogochukwu Ubah',
    author_email = 'ogochukwubah@gmail.com',
    classifiers=[
        'Environment :: Web Environment'
        'Framework :: Django'
        'Framework :: Django:: 3.0.5'
        'License :: BSD License '
        'Programming Language :: Python'
        'Programming Language :: Python :: 3.7'
    ]
)
