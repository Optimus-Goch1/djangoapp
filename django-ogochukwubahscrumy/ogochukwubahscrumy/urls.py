from django.urls import path
from . import views
app_name = 'ogochukwubahscrumy'
urlpatterns =[
    path('', views.get_grading_parameters, name='get_grading_parameters')
]