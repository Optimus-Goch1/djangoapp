import json

import boto3
from django.http import JsonResponse
from django.views.decorators.csrf import csrf_exempt

from .models import Connection, ChatMessage


@csrf_exempt
def test(request):
    return JsonResponse({"message": "hello Daud"}, status=200)


def _parse_body(body):
    body_unicode = body.decode('utf-8')
    return json.loads(body_unicode)


@csrf_exempt
def connect(request):
    body = _parse_body(request.body)
    connection_id = body['connectionId']
    Connection.objects.create(connection_id=connection_id)
    return JsonResponse({"message": "connected successfully"}, status=200)


@csrf_exempt
def disconnect(request):
    body = _parse_body(request.body)
    connection_id = body['connectionId']
    connectionID = Connection.objects.get(connection_id=connection_id)
    connectionID.delete()
    return JsonResponse({"message": "disconnected successfully"}, status=200)


def _send_to_connection(connection_id, data):
    gatewayapi = boto3.client(
        'apigatewaymanagementapi',
        endpoint_url='https://188djw47ca.execute-api.us-east-2.amazonaws.com/test/',
        region_name='us-east-2',
        aws_access_key_id='AKIAVVPWCMGD6UI3FGH6',
        aws_secret_access_key='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'
    )
    return gatewayapi.post_to_connection(ConnectionId=connection_id, Data=json.dumps(data).encode('utf-8'))


@csrf_exempt
def send_message(request):
    body = _parse_body(request.body)
    username = body["body"]["username"]
    message = body["body"]["content"]
    timestamp = body["body"]["timestamp"]

    chat_message = ChatMessage(
        username=username,
        message=message,
        timestamp=timestamp
    )
    chat_message.save()
    connections = Connection.objects.all()
    data = {"messages": [body]}
    for connection in connections:
        connection_id = connection.connection_id
        _send_to_connection(connection_id=connection_id, data=data)
    return JsonResponse({"message": "message sent successfully!"}, status=200)


def get_recent_messages(request):
    messages = ChatMessage.objects.all().order_by('-id')
    messages_list = []
    connections = Connection.objects.all()

    for message in messages:
        message_dict = {"username": message.username, "content": message.content, "timestamp": message.timestamp}
        messages_list.apppend(message_dict)
    data = {"message": messages_list}

    for connection_id in connections:
        _send_to_connection(connection_id, data)
    return JsonResponse(data, safe=False)
