import random

from django.contrib.auth.models import Group
from django.contrib.auth.models import User
from django.http import HttpResponse
from django.shortcuts import render

from .forms import SignupForm, CreateGoalForm, MoveGoalForm
from .models import GoalStatus
from .models import ScrumyGoals

users_developer = Group.objects.get(name='Developer').user_set.all()
users_quality_assurance = Group.objects.get(name='Quality Assurance').user_set.all()
users_owner = Group.objects.get(name='Owner').user_set.all()
users_admin = Group.objects.get(name='Admin').user_set.all()

weekly_goal = GoalStatus.objects.get(status_name='Weekly Goal')
done_goal = GoalStatus.objects.get(status_name='Done Goal')
verify_goal = GoalStatus.objects.get(status_name='Verify Goal')
daily_goal = GoalStatus.objects.get(status_name='Daily Goal')


def index(request):
    form = SignupForm()
    if request.method == 'POST':
        form = SignupForm(request.POST)
        if form.is_valid():
            new_user = form.save()
            new_user.set_password(request.POST.get('password'))
            my_group = Group.objects.get(name='Developer')
            my_group.user_set.add(new_user)
            new_user.save()
            return HttpResponse("Your account has been created successfully")
    else:
        form = SignupForm()
    return render(request, 'ogochukwubahscrumy/index.html', {'form': form})


def home(request):
    context = {
        'Users': User.objects.all(),
        'WeeklyGoal': weekly_goal.scrumygoals_set.all(),
        'DailyGoal': daily_goal.scrumygoals_set.all(),
        'VerifyGoal': verify_goal.scrumygoals_set.all(),
        'DoneGoal': done_goal.scrumygoals_set.all(),
        'scrumy_goal': ScrumyGoals.objects.all()
    }
    return render(request, 'ogochukwubahscrumy/home.html', context)


def add_goal(request):
    number = random.randint(1000, 10000)
    goal_id_list = []
    if number not in goal_id_list:
        id = number
        goal_id_list.append(number)

    current_user = request.user

    if request.method == 'POST':
        form = CreateGoalForm(request.POST)
        if form.is_valid():
            if current_user in users_developer:
                if current_user != form.cleaned_data.get('user'):
                    return HttpResponse(
                        f"You are not authorised to create a new goal for {form.cleaned_data.get('user')}")
                else:
                    new_goal = form.save(commit=False)
                    new_goal.goal_id = id
                    new_goal.goal_status = weekly_goal
                    new_goal.user = current_user
                    new_goal.save()
                    return HttpResponse("You have successfully created a new goal")

            elif current_user in users_quality_assurance:
                if current_user != form.cleaned_data.get('user'):
                    return HttpResponse(
                        f"You are not authorised to create a new goal for {form.cleaned_data.get('user')}")
                else:
                    new_goal = form.save(commit=False)
                    new_goal.goal_id = id
                    new_goal.goal_status = weekly_goal
                    new_goal.user = current_user
                    new_goal.save()
                    return HttpResponse("You have successfully created a new goal")

            elif current_user in users_owner:
                new_goal = form.save(commit=False)
                new_goal.goal_id = id
                new_goal.goal_status = weekly_goal
                new_goal.user = form.cleaned_data.get('user')
                new_goal.save()
                return HttpResponse("You have successfully created a new goal")
    else:
        form = CreateGoalForm()
    return render(request, 'ogochukwubahscrumy/addgoal.html', {'form': form})


def move_goal(request, goal_id):
    try:
        goal = ScrumyGoals.objects.get(goal_id=goal_id)
        form = MoveGoalForm()
        current_user = request.user

        if request.method == 'POST':
            form = MoveGoalForm(request.POST)
            if form.is_valid():
                if current_user in users_developer:
                    if goal.user != current_user:
                        return HttpResponse(f"You are not authorised to move {goal.user}'s goals")
                    goal.goal_name = goal.goal_name
                    if form.cleaned_data.get('goal_status') == done_goal:
                        return HttpResponse(f"You are not authorised to move {goal.goal_name} to"
                                            f" {form.cleaned_data.get('goal_status')}")
                    else:
                        goal.goal_status = form.cleaned_data.get('goal_status')
                        goal.save()
                        return HttpResponse(f"You have successfully moved {goal.goal_name} to {goal.goal_status}")

                elif current_user in users_quality_assurance:
                    goal.goal_name = goal.goal_name

                    if goal.user == current_user:
                        goal.goal_status = form.cleaned_data.get('goal_status')
                        goal.save()
                        return HttpResponse(f"You have successfully moved {goal.goal_name} to {goal.goal_status}")

                    elif goal.user != current_user:
                        if goal.goal_status == verify_goal:
                            if form.cleaned_data.get('goal_status') != done_goal:
                                return HttpResponse(
                                    f"You are not authorised to move {goal.goal_status} to"
                                    f" {form.cleaned_data.get('goal_status')}")
                            else:
                                goal.goal_status = form.cleaned_data.get('goal_status')
                                goal.save()
                            return HttpResponse(f"You have successfully moved {goal.goal_name} to {goal.goal_status}")
                        else:
                            return HttpResponse(f"You are not authorised to move {goal.goal_name} to {goal.goal_status}")

                elif current_user in users_admin:
                    goal.goal_name = goal.goal_name
                    goal.user = goal.user
                    goal.goal_status = form.cleaned_data.get('goal_status')
                    goal.save()
                    return HttpResponse(f"You have successfully moved {goal.goal_name} to {goal.goal_status}")

                elif current_user in users_owner:
                    goal.goal_name = goal.goal_name
                    if goal.user == current_user:
                        goal.goal_status = form.cleaned_data.get('goal_status')
                        goal.save()
                        return HttpResponse(f"You have successfully moved {goal.goal_name} to {goal.goal_status}")
                    else:
                        return HttpResponse(f"You are not authorised to move {goal.user}'s goals")
            else:
                form = MoveGoalForm()

    except ScrumyGoals.DoesNotExist:
        return render(request, 'ogochukwubahscrumy/exception.html',
                      {'error': 'A record with that goal id does not exist'})

    return render(request, 'ogochukwubahscrumy/movegoal.html', {'form': form})

# Create your views here.
